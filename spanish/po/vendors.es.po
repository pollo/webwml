# Fernando C. Estrada <fcestrada@fcestrada.com>, 2012.
msgid ""
msgstr ""
"Project-Id-Version: vendors webwml\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2015-07-02 00:57+0100\n"
"Last-Translator: Laura Arjona Reina <larjona@larjona.net>\n"
"Language-Team: Debian Spanish Translation Team <debian-l10n-spanish@lists."
"debian.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/CD/vendors/vendors.CD.def:14
msgid "Vendor"
msgstr "Vendedor"

#: ../../english/CD/vendors/vendors.CD.def:15
msgid "Allows Contributions"
msgstr "Acepta donaciones"

#: ../../english/CD/vendors/vendors.CD.def:16
msgid "CD/DVD/BD/USB"
msgstr "CD/DVD/BD/USB"

#: ../../english/CD/vendors/vendors.CD.def:17
msgid "Architectures"
msgstr "Arquitecturas"

#: ../../english/CD/vendors/vendors.CD.def:18
msgid "Ship International"
msgstr "Envíos internacionales"

#: ../../english/CD/vendors/vendors.CD.def:19
msgid "Contact"
msgstr "Contacto"

#. ###################
#. Vendor home page link, Debian page link
#: ../../english/CD/vendors/vendors.CD.def:44
msgid "Vendor Home"
msgstr "Sitio del vendedor"

#: ../../english/CD/vendors/vendors.CD.def:70
msgid "page"
msgstr "página"

#: ../../english/CD/vendors/vendors.CD.def:71
msgid "email"
msgstr "correo electrónico"

#: ../../english/CD/vendors/vendors.CD.def:114
msgid "within Europe"
msgstr "dentro de Europa"

#: ../../english/CD/vendors/vendors.CD.def:118
msgid "To some areas"
msgstr "A algunas áreas"

#: ../../english/CD/vendors/vendors.CD.def:122
msgid "source"
msgstr "fuente"

#: ../../english/CD/vendors/vendors.CD.def:126
msgid "and"
msgstr "y"

#~ msgid "Vendor:"
#~ msgstr "Vendedor:"

#~ msgid "URL for Debian Page:"
#~ msgstr "URL para la página de Debian:"

#~ msgid "Allows Contribution to Debian:"
#~ msgstr "Acepta donaciones para Debian:"

#~ msgid "Country:"
#~ msgstr "País:"

#~ msgid "Ship International:"
#~ msgstr "Envíos Internacionales:"

#~ msgid "email:"
#~ msgstr "correo electrónico:"

#~ msgid "CD Type:"
#~ msgstr "Tipo de CD:"

#~ msgid "DVD Type:"
#~ msgstr "Tipo de DVD:"

#~ msgid "BD Type:"
#~ msgstr "Tipo de BD:"

#~ msgid "USB Type:"
#~ msgstr "Tipo de USB:"

#~ msgid "Architectures:"
#~ msgstr "Arquitecturas:"

# German translation of the Debian webwml modules
# Dr. Tobias Quathamer <toddy@debian.org>, 2011, 2012, 2016, 2017.
# Holger Wansing <linux@wansing-online.de>, 2011, 2012.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml templates\n"
"PO-Revision-Date: 2017-05-15 22:58+0100\n"
"Last-Translator: Dr. Tobias Quathamer <toddy@debian.org>\n"
"Language-Team: German <debian-l10n-german@lists.debian.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Übersetzungsstatistik der Debian-Website"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Es sind %d Seiten zu übersetzen."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Es sind %d Byte zu übersetzen."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Es sind %d Zeichenketten zu übersetzen."

#: ../../stattrans.pl:282 ../../stattrans.pl:506
msgid "Wrong translation version"
msgstr "Falsche Version übersetzt"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Diese Übersetzung ist zu alt"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Das Original ist aktueller als diese Übersetzung"

#: ../../stattrans.pl:290 ../../stattrans.pl:506
msgid "The original no longer exists"
msgstr "Das Original existiert nicht mehr"

#: ../../stattrans.pl:482
msgid "hit count N/A"
msgstr "Keine Zugriffszahlen"

#: ../../stattrans.pl:482
msgid "hits"
msgstr "Zugriffe"

#: ../../stattrans.pl:500 ../../stattrans.pl:501
msgid "Click to fetch diffstat data"
msgstr "Klicken Sie, um diffstat-Daten zu erhalten"

#: ../../stattrans.pl:516 ../../stattrans.pl:679 ../../stattrans.pl:680
msgid "Unified diff"
msgstr "Einheitliche Unterschiede"

#: ../../stattrans.pl:519 ../../stattrans.pl:679 ../../stattrans.pl:680
msgid "Colored diff"
msgstr "Farbige Unterschiede"

#: ../../stattrans.pl:524 ../../stattrans.pl:683
#, fuzzy
#| msgid "Colored diff"
msgid "Commit diff"
msgstr "Farbige Unterschiede"

#. FIXME - this is clearly wrong, but no idea what's meant to go here!
#: ../../stattrans.pl:528 ../../stattrans.pl:683
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:639 ../../stattrans.pl:785
msgid "Created with <transstatslink>"
msgstr "Erstellt mit <transstatslink>"

#: ../../stattrans.pl:644
msgid "Translation summary for"
msgstr "Zusammenfassung der Übersetzung für"

#: ../../stattrans.pl:647 ../../stattrans.pl:809 ../../stattrans.pl:855
#: ../../stattrans.pl:898
msgid "Not translated"
msgstr "Nicht übersetzt"

#: ../../stattrans.pl:647 ../../stattrans.pl:808 ../../stattrans.pl:854
msgid "Outdated"
msgstr "Veraltet"

#: ../../stattrans.pl:647
msgid "Translated"
msgstr "Übersetzt"

#: ../../stattrans.pl:647 ../../stattrans.pl:733 ../../stattrans.pl:807
#: ../../stattrans.pl:853 ../../stattrans.pl:896
msgid "Up to date"
msgstr "Aktuell"

#: ../../stattrans.pl:648 ../../stattrans.pl:649 ../../stattrans.pl:650
#: ../../stattrans.pl:651
msgid "files"
msgstr "Dateien"

#: ../../stattrans.pl:654 ../../stattrans.pl:655 ../../stattrans.pl:656
#: ../../stattrans.pl:657
msgid "bytes"
msgstr "Byte"

#: ../../stattrans.pl:664
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Beachten Sie: Die Auflistung der Seiten ist nach der Häufigkeit der Abrufe "
"sortiert. Bewegen Sie den Mauszeiger auf den Seitennamen, um die Anzahl der "
"Zugriffe zu sehen."

#: ../../stattrans.pl:670
msgid "Outdated translations"
msgstr "Veraltete Übersetzungen"

#: ../../stattrans.pl:672 ../../stattrans.pl:732
msgid "File"
msgstr "Datei"

#: ../../stattrans.pl:674 ../../stattrans.pl:681
msgid "Diff"
msgstr "Unterschiede"

#: ../../stattrans.pl:676
msgid "Comment"
msgstr "Kommentar"

#: ../../stattrans.pl:677
msgid "Diffstat"
msgstr "Diffstat"

#: ../../stattrans.pl:686
msgid "Log"
msgstr "Protokoll"

#: ../../stattrans.pl:687
msgid "Translation"
msgstr "Übersetzung"

#: ../../stattrans.pl:688
msgid "Maintainer"
msgstr "Betreuer"

#: ../../stattrans.pl:690
msgid "Status"
msgstr "Status"

#: ../../stattrans.pl:691
msgid "Translator"
msgstr "Übersetzer"

#: ../../stattrans.pl:692
msgid "Date"
msgstr "Datum"

#: ../../stattrans.pl:699
msgid "General pages not translated"
msgstr "Allgemeine Seiten, die nicht übersetzt sind"

#: ../../stattrans.pl:700
msgid "Untranslated general pages"
msgstr "Nicht übersetzte allgemeine Seiten"

#: ../../stattrans.pl:705
msgid "News items not translated"
msgstr "Nachrichten, die nicht übersetzt sind"

#: ../../stattrans.pl:706
msgid "Untranslated news items"
msgstr "Nicht übersetzte Nachrichten"

#: ../../stattrans.pl:711
msgid "Consultant/user pages not translated"
msgstr "Berater-/Benutzerseiten, die nicht übersetzt sind"

#: ../../stattrans.pl:712
msgid "Untranslated consultant/user pages"
msgstr "Nicht übersetzte Berater-/Benutzerseiten"

#: ../../stattrans.pl:717
msgid "International pages not translated"
msgstr "Internationale Seiten, die nicht übersetzt sind"

#: ../../stattrans.pl:718
msgid "Untranslated international pages"
msgstr "Nicht übersetzte internationale Seiten"

#: ../../stattrans.pl:723
msgid "Translated pages (up-to-date)"
msgstr "Übersetzte Seiten (aktuell)"

#: ../../stattrans.pl:730 ../../stattrans.pl:880
msgid "Translated templates (PO files)"
msgstr "Übersetzte Vorlagen (PO-Dateien)"

#: ../../stattrans.pl:731 ../../stattrans.pl:883
msgid "PO Translation Statistics"
msgstr "Statistik der PO-Übersetzung"

#: ../../stattrans.pl:734 ../../stattrans.pl:897
msgid "Fuzzy"
msgstr "Ungenau"

#: ../../stattrans.pl:735
msgid "Untranslated"
msgstr "Nicht übersetzt"

#: ../../stattrans.pl:736
msgid "Total"
msgstr "Insgesamt"

#: ../../stattrans.pl:753
msgid "Total:"
msgstr "Insgesamt:"

#: ../../stattrans.pl:787
msgid "Translated web pages"
msgstr "Übersetzte Webseiten"

#: ../../stattrans.pl:790
msgid "Translation Statistics by Page Count"
msgstr "Übersetzungsstatistik nach der Seitenanzahl"

#: ../../stattrans.pl:805 ../../stattrans.pl:851 ../../stattrans.pl:895
msgid "Language"
msgstr "Sprache"

#: ../../stattrans.pl:806 ../../stattrans.pl:852
msgid "Translations"
msgstr "Übersetzungen"

#: ../../stattrans.pl:833
msgid "Translated web pages (by size)"
msgstr "Übersetzte Webseiten (nach Größe)"

#: ../../stattrans.pl:836
msgid "Translation Statistics by Page Size"
msgstr "Übersetzungsstatistik nach der Seitengröße"

#~ msgid "Hit data from %s, gathered %s."
#~ msgstr "Zugriffsdaten von %s, erfasst am %s."

#~ msgid "Origin"
#~ msgstr "Original"

#~ msgid "Created with"
#~ msgstr "Erzeugt mit"

# translation of bugs.po to தமிழ்
# ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: bugs\n"
"PO-Revision-Date: 2007-11-07 16:42+0530\n"
"Last-Translator: ஸ்ரீராமதாஸ் <shriramadhas@gmail.com>\n"
"Language-Team: தமிழ் <ubuntu-l10n-tam@lists.ubuntu.com>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: ../../english/Bugs/pkgreport-opts.inc:17
msgid "in package"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:20
#: ../../english/Bugs/pkgreport-opts.inc:60
#: ../../english/Bugs/pkgreport-opts.inc:94
msgid "tagged"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:23
#: ../../english/Bugs/pkgreport-opts.inc:63
#: ../../english/Bugs/pkgreport-opts.inc:97
#, fuzzy
#| msgid "severities"
msgid "with severity"
msgstr "முன்னுரிமைகள்"

#: ../../english/Bugs/pkgreport-opts.inc:26
msgid "in source package"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:29
msgid "in packages maintained by"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:32
msgid "submitted by"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:35
msgid "owned by"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:38
#, fuzzy
#| msgid "status"
msgid "with status"
msgstr "நிலை"

#: ../../english/Bugs/pkgreport-opts.inc:41
msgid "with mail from"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:44
msgid "newest bugs"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:57
#: ../../english/Bugs/pkgreport-opts.inc:91
msgid "with subject containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:66
#: ../../english/Bugs/pkgreport-opts.inc:100
msgid "with pending state"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:69
#: ../../english/Bugs/pkgreport-opts.inc:103
msgid "with submitter containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:72
#: ../../english/Bugs/pkgreport-opts.inc:106
msgid "with forwarded containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:75
#: ../../english/Bugs/pkgreport-opts.inc:109
msgid "with owner containing"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:78
#: ../../english/Bugs/pkgreport-opts.inc:112
msgid "with package"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:122
msgid "normal"
msgstr "சாதாரண"

#: ../../english/Bugs/pkgreport-opts.inc:125
msgid "oldview"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:128
msgid "raw"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:131
#, fuzzy
#| msgid "sarge"
msgid "age"
msgstr "சார்ஜ்"

#: ../../english/Bugs/pkgreport-opts.inc:137
msgid "Repeat Merged"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:138
msgid "Reverse Bugs"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:139
msgid "Reverse Pending"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:140
#, fuzzy
#| msgid "Reverse order of:"
msgid "Reverse Severity"
msgstr "மாற்று வரிசையில்:"

#: ../../english/Bugs/pkgreport-opts.inc:141
msgid "No Bugs which affect packages"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:143
msgid "None"
msgstr ""

#: ../../english/Bugs/pkgreport-opts.inc:144
msgid "testing"
msgstr "சோதிக்கப்படுகிறது"

#: ../../english/Bugs/pkgreport-opts.inc:145
msgid "oldstable"
msgstr "பழைய நிலையான"

#: ../../english/Bugs/pkgreport-opts.inc:146
msgid "stable"
msgstr "நிலையான"

#: ../../english/Bugs/pkgreport-opts.inc:147
msgid "experimental"
msgstr "பரிசோதனையில்"

#: ../../english/Bugs/pkgreport-opts.inc:148
msgid "unstable"
msgstr "நிலையற்ற"

#: ../../english/Bugs/pkgreport-opts.inc:152
#, fuzzy
#| msgid "archived bugs"
msgid "Unarchived"
msgstr "களையப் பட்ட வழுக்கள்"

#: ../../english/Bugs/pkgreport-opts.inc:155
#, fuzzy
#| msgid "archived bugs"
msgid "Archived"
msgstr "களையப் பட்ட வழுக்கள்"

#: ../../english/Bugs/pkgreport-opts.inc:158
msgid "Archived and Unarchived"
msgstr ""

#~ msgid "Exclude tag:"
#~ msgstr "விலக்குவதற்கான அடைகுறி:"

#~ msgid "Include tag:"
#~ msgstr "சேர்ப்பதற்கான அடைகுறி: "

#~ msgid "lfs"
#~ msgstr "lfs"

#~ msgid "ipv6"
#~ msgstr "ipv6"

#~ msgid "wontfix"
#~ msgstr "தீர்க்கப்படாத"

#~ msgid "upstream"
#~ msgstr "மூலவிடம்"

#~ msgid "unreproducible"
#~ msgstr "மீட்கவியலாது"

#~ msgid "security"
#~ msgstr "அரண்"

#~ msgid "patch"
#~ msgstr "தீர்வை"

#~ msgid "moreinfo"
#~ msgstr "மேலும் தகவலுக்கு"

#~ msgid "l10n"
#~ msgstr "தன்மொழியாக்கம்"

#~ msgid "help"
#~ msgstr "உதவி"

#~ msgid "fixed-upstream"
#~ msgstr "மூலவிடத்தில் சரிசெய்யபட்டது"

#~ msgid "fixed-in-experimental"
#~ msgstr "சோதனை வெளியீட்டில் சரிசெய்யப் பட்டது"

#~ msgid "d-i"
#~ msgstr "டெ-நி"

#~ msgid "confirmed"
#~ msgstr "உறுதிசெய்யப்பட்ட"

#~ msgid "sid"
#~ msgstr "சிட்"

#~ msgid "lenny-ignore"
#~ msgstr "லென்னி-தவிர்"

#~ msgid "lenny"
#~ msgstr "லென்னி"

#~ msgid "etch-ignore"
#~ msgstr "எட்ச்-தவிர்"

#~ msgid "etch"
#~ msgstr "எட்ச்"

#~ msgid "sarge-ignore"
#~ msgstr "சார்ஜ்-தவிர்"

#~ msgid "woody"
#~ msgstr "ஊடி"

#~ msgid "potato"
#~ msgstr "பொடேடோ"

#~ msgid "Exclude severity:"
#~ msgstr "முன்னுரிமையை விலக்குக:"

#~ msgid "Include severity:"
#~ msgstr "முன்னுரிமையை சேர்க்க:"

#~ msgid "wishlist"
#~ msgstr "அவாப்பட்டியல்"

#~ msgid "minor"
#~ msgstr "சிறிய"

#~ msgid "important"
#~ msgstr "முக்கியமாக"

#~ msgid "serious"
#~ msgstr "அதிமுக்கியமான"

#~ msgid "grave"
#~ msgstr "அத்தியாவசியமான"

#~ msgid "critical"
#~ msgstr "அவசியமான"

#~ msgid "Exclude status:"
#~ msgstr "நிலைமையை உள்ளடக்க வேண்டா:"

#~ msgid "Include status:"
#~ msgstr "நிலைமையை உள்ளடக்குக:"

#~ msgid "done"
#~ msgstr "பூர்த்திசெய்யப்பட்ட"

#~ msgid "fixed"
#~ msgstr "களையப்பட்ட"

#~ msgid "pending"
#~ msgstr "மீதமுள்ள"

#~ msgid "forwarded"
#~ msgstr "வழியனுப்பபட்ட"

#~ msgid "open"
#~ msgstr "திறக்க"

#~ msgid "bugs"
#~ msgstr "வழுக்கள்"

#~ msgid "Distribution:"
#~ msgstr "வழங்கல்:"

#~ msgid "Package version:"
#~ msgstr "பொதி வெளியீடு:"

#~ msgid "testing-proposed-updates"
#~ msgstr "சோதனையின் போது பரிந்துரைக்கப்பட்ட புதியவை"

#~ msgid "proposed-updates"
#~ msgstr "பரிந்துரைக்கப்பட்ப புதுமைகள்"

#~ msgid "don't show statistics in the footer"
#~ msgstr "அடிக்குறிப்பில் புள்ளிவிவரங்களைக் காட்டாதே"

#~ msgid "don't show table of contents in the header"
#~ msgstr "தலைகுறிப்பில்பில் அட்டவணையைக் காட்டாதே"

#~ msgid "no ordering by status or severity"
#~ msgstr "நிலை மற்றும் முன்னுரிமை வாரியாக வரிசைப் படுத்துவது இல்லை"

#~ msgid "display merged bugs only once"
#~ msgstr "ஒன்றிணைக்கப்பட்ட பிழைகளை ஒரு முறை மட்டும் காட்டுக"

#~ msgid "active bugs"
#~ msgstr "களையப்படா வழுக்கள்"

#~ msgid "Flags:"
#~ msgstr "குறிகள்:"
